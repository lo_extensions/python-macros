[//]: # (Copyright (c)  2018  Mauricio Baeza web [AT] correlibre [DOT] net)
[//]: # (Permission is granted to copy, distribute and/or modify this document)
[//]: # (under the terms of the GNU Free Documentation License, Version 1.3)
[//]: # (or any later version published by the Free Software Foundation;)
[//]: # (with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.)
[//]: # (A copy of the license is included in the section entitled "GNU)
[//]: # (Free Documentation License".)


<div style="text-align: right">
Divide each difficulty into as many parts<br>
as is feasible and necessary to resolve it.
<BR>
<b>Rene Descartes</b>
</div>


## Macros

<BR>
Remember... like a mantra...

<BR>
<div class="alert-box notice"><span>TIP: </span>
    Python is very strict are its syntax, caution when write or copy and paste.
</div>
<BR>


#### Write and call macros

The basic structured for a macro is.

    def name_macro():
        # Instructions for this macro
        return

<br>
Two points in last code: first and more important, in Python, the indentation
have effects in the execution and result of code, and second, every line that
start with number symbol, it's a comment.

Attention with **def** and **return**, this are we first keywords Python, that
mean, are reservered words and we not can used like identifiers or variables.

In introduction, we learned how to execute a macro. Macro by macro. But, we can
too, execute one or more macros from other. For example.

    def main():
        your_name()
        your_phone()
        return


    def your_name():
        return


    def your_phone():
        return

<br>
When we execute the macro **main**, we execute the others too.

For convenience, we can assign the execution of a macro to keyboard shortcut.

* Open Calc
* Select menu: ***View -> Toolbars -> Customize...***
* Select tab **Keyboard**
* Select radio button **LibreOffice**
* In control list box **Shortcut Keys**, select one combination keys that not be used. For example: ***CTRL+ALT+P***
* In list box **Category**, search and select our library macros.
* In list box **Function**, select macro **main**.
* And click in command button **Modify**
* Close dialog box with command button **OK**

![Shortcut Macros](img/02/img_macros_001.png)

Now, we can test our macros, with keyboard shortcut selected.

If you open dialog box **Macro Selector** (***Tools->Macros->Run Macro...***)
and select library **mymacros**, we can see, in list box **Macro Name**, the
macro names that we writer. We can controller the macros show in this list.

![Macros](img/02/img_macros_002.png)

Only add next line in the end of library.

```python
g_exportedScripts = [main, my_first_macro_calc]
```

![Macros](img/02/img_macros_003.png)


<br>
#### Show information

You can use function **print** for show any data.

    def main():
        print('Main function')
        your_name()
        your_phone()
        return


    def your_name():
        print('El Mau')
        return


    def your_phone():
        print('52 55 12345678')
        return

<br>
But, if you execute this macro from LibreOffice, you never see anything.

In GNU/Linux, you need start LibreOffice from command line and execute macro.

![Macros](img/02/img_macros_004.png)

In Windows, if you use Windows, please, replace with Linux. :)

If not it's possible, then use this function instead of function print.

```python
def msgbox(message):
    context = uno.getComponentContext()
    service_manager = context.getServiceManager()
    toolkit = service_manager.createInstance('com.sun.star.awt.Toolkit')
    parent = toolkit.getDesktopWindow()
    message_box = toolkit.createMessageBox(parent, 0, 1, 'Python Macros', message)
    message_box.execute()
    return
```

<br>
Again, don't worry, after we'll explain line by line. Of course this function
we can use in Linux too.


<br>
#### Service Manager

The services, you can see it like the way to access to API of LibreOffice.

![Macros](img/02/img_macros_005.png)

But each service, has a component context.

![Macros](img/02/img_macros_006.png)

Then, the firs step is to access the component context. We have two way for this.

    import uno


    def main():
        get_component_context()
        return


    def get_component_context():
        # ~ Use global variable
        ctx1 = XSCRIPTCONTEXT.getComponentContext()

        # ~ Use a method
        ctx2 = uno.getComponentContext()

        # ~ Compare
        msgbox(ctx1 == ctx2)
        return

![Macros](img/02/img_macros_007.png)

Now, we can access to the service factory.

```python
    context = uno.getComponentContext()
    service_manager = context.getServiceManager()
```

But, how to know, that this **context** have method **getServiceManager**.

One way is, to see the documentation for this service: [com.sun.star.uno.XComponentContext][1].

The best way is for the **object** itself to tell us ***what know it to do***.
For this, we use the extension [MRI](introduction/#utilities), with the next
function.

    import uno


    def main():
        get_component_context()
        return


    def get_component_context():
        ctx = uno.getComponentContext()
        mri(ctx)
        return


    def mri(target):
        ctx = uno.getComponentContext()
        sm = ctx.getServiceManager()
        tool_mri = sm.createInstanceWithContext('mytools.Mri', ctx)
        tool_mri.inspect(target)
        return

![Macros](img/02/img_macros_008.png)

Now, we compare the three first lines of the functions:

msgbox | mri |
------------ | ------------- |
ctx = uno.getComponentContext() | ctx = uno.getComponentContext() |
sm = context.getServiceManager() | sm = ctx.getServiceManager() |
toolkit = sm.**createInstance**('com.sun.star.awt.Toolkit') | tool_mri = sm.**createInstanceWithContext**('mytools.Mri', ctx) |

We noted that in the last line, we use different methods, for function **msgbox**
**createInstance**, and in **mri** function **createInstanceWithContext**. In
this last function, always is necessary pass argument **context**.

Other service that need **context** for maker, it's [Desktop service][2], an
important service because it's used for open documents from disk, make new
documents or can access current open documents. Desktop it's of the root for
all documents.

![Macros](img/02/img_macros_009.png)

A little test of this service.

    import uno


    def main():
        test_desktop()
        return


    def test_desktop():
        ctx = uno.getComponentContext()
        sm = ctx.getServiceManager()
        desktop = sm.createInstanceWithContext('com.sun.star.frame.Desktop', ctx)
        new_doc = desktop.loadComponentFromURL('private:factory/scalc', '_default', 0, ())
        return

<br>
Later we'll see details for this service.

Now, we know make macros of two ways. Without arguments, like **main**, and with
one argument, like **msgbox** and **mri**.

If we see the functions: **msgbox**, **mri** and **test_desktop**, we note that,
all use the same first three lines. So, we can turn this lines in to a function.

First version.

    import uno


    def main():
        msgbox('Divide and conquer')
        return


    def create_service(name):
        context = uno.getComponentContext()
        service_manager = context.getServiceManager()
        service = service_manager.createInstance(name)
        return service


    def msgbox(message):
        toolkit = create_service('com.sun.star.awt.Toolkit')
        parent = toolkit.getDesktopWindow()
        message_box = toolkit.createMessageBox(parent, 0, 1, 'Python Macros', message)
        message_box.execute()
        return

<br>
Remember...
<BR>
<div class="alert-box notice"><span>TIP: </span>
    In Python, the indentation have effects in the execution and result of code.
</div>
<BR>

But, the functions: **mri** and **test_desktop**, use it the function
**createInstanceWithContext**, for fix this, in this second version of our
macro, we pass a second argument.

    import uno


    def main():
        msgbox('Divide and conquer')
        doc = test_desktop()
        mri(doc)
        return


    def create_service(name, with_context):
        context = uno.getComponentContext()
        service_manager = context.getServiceManager()
        if with_context:
            service = service_manager.createInstanceWithContext(name, context)
        else:
            service = service_manager.createInstance(name)
        return service


    def msgbox(message):
        toolkit = create_service('com.sun.star.awt.Toolkit', False)
        parent = toolkit.getDesktopWindow()
        message_box = toolkit.createMessageBox(parent, 0, 1, 'Python Macros', message)
        message_box.execute()
        return


    def mri(target):
        tool_mri = create_service('mytools.Mri', True)
        tool_mri.inspect(target)
        return


    def test_desktop():
        desktop = create_service('com.sun.star.frame.Desktop', True)
        new_doc = desktop.loadComponentFromURL('private:factory/scalc', '_default', 0, ())
        return new_doc

<br>
Much better. Happy develop.


[1]: https://api.libreoffice.org/docs/idl/ref/interfacecom_1_1sun_1_1star_1_1uno_1_1XComponentContext.html
[2]: https://api.libreoffice.org/docs/idl/ref/servicecom_1_1sun_1_1star_1_1frame_1_1Desktop.html
