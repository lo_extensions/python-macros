[//]: # (Copyright (c)  2018  Mauricio Baeza web [AT] correlibre [DOT] net)
[//]: # (Permission is granted to copy, distribute and/or modify this document)
[//]: # (under the terms of the GNU Free Documentation License, Version 1.3)
[//]: # (or any later version published by the Free Software Foundation;)
[//]: # (with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.)
[//]: # (A copy of the license is included in the section entitled "GNU)
[//]: # (Free Documentation License".)


<div style="text-align: right">
He who receives an idea from me,<BR>
receives instruction himself without lessening mine;<BR>
as he who lights his taper at mine,<BR>
receives light without darkening me.<BR>
<BR>
<b>Thomas Jefferson</b>
</div>


# Welcome to Python Macros

Develop macros in [LibreOffice][1] with [Python][2] by examples.

## Content:

1. [Introduction](introduction.md)
1. [Macros](macros.md)
1. [Calc functions](functions.md)
1. [Extensions](extensions.md)
1. New components

[1]: https://libreoffice.org
[2]: https://python.org


