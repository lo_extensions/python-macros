[//]: # (Copyright (c)  2018  Mauricio Baeza web [AT] correlibre [DOT] net)
[//]: # (Permission is granted to copy, distribute and/or modify this document)
[//]: # (under the terms of the GNU Free Documentation License, Version 1.3)
[//]: # (or any later version published by the Free Software Foundation;)
[//]: # (with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.)
[//]: # (A copy of the license is included in the section entitled "GNU)
[//]: # (Free Documentation License".)


## Introduction

<BR>
Develop macros in LibreOffice with Python, it's fast and easy. If this
documentation can be help you, I'm will be very happy.
<BR>
<div class="alert-box notice"><span>TIP: </span>
    We recommended, always used LibreOffice Still for develop and production.
</div>


<BR>
#### Test support for Python macros

<BR>
<div class="alert-box notice"><span>TIP: </span>
    In some Linux distributions, you need install the extension for support python scripts.
</div>

<BR>

* LinuxMint `sudo apt install libreoffice-script-provider-python`


<BR>

* Open new document Writer
* Select menu: **Tools -> Macro -> Run macros...**
* Select Library: **LibreOffice Macros** -> **HelloWorld**
* Select macro **HelloWorldPython**
* Click in command button **Run**
![Run Macros](img/01/img_intro_01.png)

<BR>
* If you see like next image, your system can run Python macros.
![Test Python Macros](img/01/img_intro_02.png)

<BR>
#### Where save my macros?

* GNU/Linux
    * For all users:
        * `/usr/lib/libreoffice/share/Scripts/python/`
    * For current user:
        * `~/.config/libreoffice/4/user/Scripts/python`
* Windows
    * For all users:
        * `C:\Program Files\LibreOffice 5\share\Scripts\python`
    * For current user:
        * `C:\Users\Virtual\AppData\Roaming\LibreOffice\4\user\Scripts\python`


<BR>

<div class="alert-box warning"><span>warning: </span>
    The following procedure may leave the file broken, always create a backup file.
</div>

<BR>

* Inside a document

    * Save new document, for example, a document Writer and close.
    * Copy file and rename extension to **zip**.

            .
            ├── MyMacros.odt
            └── MyMacros.zip

    * Open file **zip** with your prefer software for this type files.
    * Into root, create folders `Scripts/python`
    * Copy file with macros inside this folder.
        ![ZIP File](img/01/img_intro_03.png)
    * In root folder, edit file **manifest.xml** inside folder **META-INF**
        ![ZIP File](img/01/img_intro_04.png)
    * Add this lines, just before last tag `</manifest:manifest>`


                <manifest:file-entry manifest:media-type="" manifest:full-path="Scripts/python/mymacros.py"/>
                <manifest:file-entry manifest:media-type="application/binary" manifest:full-path="Scripts/python/"/>
                <manifest:file-entry manifest:media-type="application/binary" manifest:full-path="Scripts/"/>
            </manifest:manifest>

    * Save and close.
    * Delete origin file ODT and rename file **zip** to **odt**.
    * Open file and activate macros.
    * Select menu Tools -> Macro -> Run macros...
    * If you see the library (*mymacros*) and the macro, congratulations!
        ![Macros inside document](img/01/img_intro_05.png)


After we'll see how to automatize this.

<BR>
#### How to organize macros?
<BR>
Each file with extension **py**, it's a library and each function it's a macro.
More later we'll learn that we can organize it in classes too.


<BR>
#### Our first macro
<BR>
<div class="alert-box warning"><span>warning: </span>
    Python is very strict are its syntax, caution when write or copy and paste.
</div>
<BR>

* Inside folder macros for current user, remember:
    * GNU/Linux
        * `~/.config/libreoffice/4/user/Scripts/python`
    * Windows
        * `C:\Users\Virtual\AppData\Roaming\LibreOffice\4\user\Scripts\python`
* Create file **mymacros.py**, this file is our first macro library.
* With your favorite plain text editor, write next macro, yes, you can copy and paste.

        import uno

        def my_first_macro_writer():
            doc = XSCRIPTCONTEXT.getDocument()
            text = doc.getText()
            text.setString('Hello World Writer, with Python')
            return

* Don't worry, we'll explain every line later.
* Open new document Writer.
* Select menu: **Tools -> Macro -> Run macros...**
* Select Library: **My Macros** -> **mymacros**
* Select macro **my_first_macro_writer**
![My first macro Writer](img/01/img_intro_06.png)
* Click in command button **Run**
![My first macro Writer](img/01/img_intro_07.png)

<BR>

* Now, the same but with Calc.
* In the same file, you write.

        import uno


        def my_first_macro_writer():
            doc = XSCRIPTCONTEXT.getDocument()
            text = doc.getText()
            text.setString('Hello World Writer, with Python')
            return


        def my_first_macro_calc():
            doc = XSCRIPTCONTEXT.getDocument()
            cell = doc.Sheets[0]['A1']
            cell.setString('Hello World Calc, with Python')
            return


<BR>

* Of course, now open document Calc
![My first macro Calc](img/01/img_intro_08.png)
* And execute.
![My first macro Calc](img/01/img_intro_09.png)


<BR>
#### More frequently errors

Python it's very strict with the syntax, so, when we started develop macros,
not will see the name of macro in dialog Macro Selector, if some macro have
syntax error, for example, the next macro:

        def macro_test_error():
        return

Not see macros
![Macro error](img/01/img_intro_10.png)

But, if macro not have error.

        def macro_test_error():
            return

![Macro without error](img/01/img_intro_11.png)


<BR>
#### What IDE we'll used?

[Geany][1] it's a good option and we used it for write this documentation, but you
can used your favorite IDE for python code.
![Geany](img/01/img_intro_12.png)

It's very important, setting Geany (CTRL+ALT+P) for:

* Used space like indentation.
![Geany](img/01/img_intro_17.png)

* Autocomplete.
![Geany](img/01/img_intro_18.png)

* Display some elements.
![Geany](img/01/img_intro_19.png)

* When saved files, replace tabs with space.
![Geany](img/01/img_intro_20.png)


<BR>
#### Utilities

[MRI][2] must be it's the most important extension wend we develop macros,
download and install it.

![Geany](img/01/img_intro_13.png)

![Geany](img/01/img_intro_14.png)

After restart LibreOffice, you must see a new menu Add-Ons in menu Tools.

![Geany](img/01/img_intro_15.png)

Select MRI

![Geany](img/01/img_intro_16.png)

We'll used this tool, all time.


<BR>
#### LibreOffice SDK

If you want develop: Calc functions or new UNO components, you need install
[LibreOffice SDK][3].

<BR>

<div class="alert-box warning"><span>warning: </span>
    Do not mix and match LibreOffice releases with downstream SDK packages or
    vice versa!
</div>

<BR>

* For ArchLinux:
    * `sudo pacman -S libreoffice-still-sdk`


[1]: https://geany.org/
[2]: https://extensions.openoffice.org/project/MRI
[3]: https://api.libreoffice.org/docs/install.html
